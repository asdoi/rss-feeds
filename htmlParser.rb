require 'html2rss'


module HtmlParser
  class Error < StandardError; end

  projects = Html2rss.feed_from_yaml_config('WebsiteFeeds.yml', 'projects-feed')
  news = Html2rss.feed_from_yaml_config('WebsiteFeeds.yml', 'news-feed')

  File.open("public/flxholleProjects.xml", "w") {|f| f.write(projects.to_s) }
  File.open("public/flxholleNews.xml", "w") {|f| f.write(news.to_s) }


  gymwen = Html2rss.feed_from_yaml_config('GymWen.yml', 'startseite')
  File.open("public/GymWenStartseite.xml", "w") {|f| f.write(gymwen.to_s) }
end
